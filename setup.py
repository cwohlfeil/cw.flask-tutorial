import io

from setuptools import find_packages, setup

with io.open('README.md', 'rt', encoding='utf8') as f:
    readme = f.read()

setup(
    name='flask-tutorial',
    version='1.0.0',
    url='http://gitlab.com/cwohlfeil/flask-tutorial',
    license='BSD',
    maintainer='Cameron Wohlfeil',
    maintainer_email='cwohlfeil@protonmail.ch',
    description='The basic blog app built in the Flask tutorial.',
    long_description=readme,
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask',
    ],
    extras_require={
        'test': [
            'pytest',
            'coverage',
        ],
    },
)